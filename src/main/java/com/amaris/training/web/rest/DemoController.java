package com.amaris.training.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api")
public class DemoController {

    @Autowired
    private Service2 service2;

    @GetMapping("/test")
    public String testAPI() {
        return "This is demo version 2";
    }

    @GetMapping("/test-2")
    public String testAPI2() {
        return "This is demo version 2" +  service2.getTest();
    }

    @GetMapping("/test-3")
    public String testAPI3() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        final String baseUrl = "http://149.28.157.88:8081/api/test-2";
        URI uri = new URI(baseUrl);
        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
        return "abc " + result.getBody().toString();
    }
}
