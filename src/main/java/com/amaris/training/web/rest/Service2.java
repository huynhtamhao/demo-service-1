package com.amaris.training.web.rest;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

// @FeignClient(value = "jplaceholder", url = "http://localhost:8081")//"http://149.28.157.88:8081")
@FeignClient(value = "jplaceholder", url = "http://149.28.157.88:8081")
public interface Service2 {

    @RequestMapping(method = RequestMethod.GET, value = "/api/test-2", produces = "application/json")
    String getTest();
}
